<?php
/**
 * Plugin Name: 1 WP GIT test plugin
 * Plugin URI: https://bitbucket.org/Alex-Moise/wp-git-test-plugin/
 * BitBucket Plugin URI: https://bitbucket.org/Alex-Moise/wp-git-test-plugin/
 * Description: A custom plugin meant to serve as a test for WP - BitBuket GIT integration.
 * Version: 1.1.0
 * Author: Alex Moise
 * Author URI: https://moise.pro
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }


// let's define a shortcode to use anywhere ...
add_shortcode( 'testgit', 'mogtest_test_git_shortcode' );
function mogtest_test_git_shortcode(){
	
	$plugin_version = '1.1.0';
	$plugin_branch = 'master';

	$html_output = '
		<div class="git_test_title">
			<h2>GIT Test control info</h2>
		</div>
		<div class="git_test">
			<span class="version"><strong>Version:</strong> '.$plugin_version.'</span>
			<span class="branch"><strong>Branch:</strong> '.$plugin_branch.'</span>
		</div>
	';
	return $html_output;
}

// ... and few styles to make it look good.
add_action('wp_head', 'mogtest_test_git_styles');
function mogtest_test_git_styles() {
	echo '
	<style>
		.git_test_title, .git_test { text-align: center; }
		.git_test { margin-bottom: 40px; }
		
	<style>
	';
}
?>
